package com.cognizant.moviecruiser.model;

import java.util.List;

public class Favorite {

	private List<Movie> movieList;
	private int count;

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
