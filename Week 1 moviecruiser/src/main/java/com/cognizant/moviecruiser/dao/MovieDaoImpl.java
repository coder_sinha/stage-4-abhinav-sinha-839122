package com.cognizant.moviecruiser.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.moviecruiser.model.Favorite;
import com.cognizant.moviecruiser.model.Movie;

@Component
public class MovieDaoImpl implements MovieDao {

	private static ApplicationContext context = new ClassPathXmlApplicationContext("movie.xml");
	private static List<Movie> movieList;
	private static Map<String,List<Movie>> favoriteList;
	public MovieDaoImpl() {
		super();
		// TODO Auto-generated constructor stub
		movieList = context.getBean("movieList",ArrayList.class);
		favoriteList = new HashMap<String, List<Movie>>();
	}

	public List<Movie> getAllMovies() {
		// TODO Auto-generated method stub
		return movieList;
	}

	public void editMovieAdmin(Movie movie) {
		// TODO Auto-generated method stub
		Iterator iterator = movieList.iterator();
		
		while(iterator.hasNext()) {
			Movie tempMovie = (Movie) iterator.next();
			if(tempMovie.equals(movie)) {
				movieList.set(movieList.indexOf(tempMovie),movie);
				break;
			}
		}
		
	}

	public List<Movie> relevantMovies() {
		// TODO Auto-generated method stub
		List<Movie> relevantMovieList = new ArrayList<Movie>();
		Date date = new Date();
		Iterator iterator = movieList.iterator();
		while(iterator.hasNext()) {
			Movie movie = (Movie) iterator.next();
			if(movie.getDateOfLaunch().before(date) && movie.isActive()) {
				relevantMovieList.add(movie);
			}
		}
		return relevantMovieList;
	}

	public void AddFavorite(String userId, int movieId) {
		// TODO Auto-generated method stub
		if(favoriteList.isEmpty() || !favoriteList.containsKey(userId)) {
			Movie tempMovie = new Movie();
			tempMovie.setId(movieId);
			List<Movie> favMovieList = new ArrayList<Movie>();
			Iterator iterator = movieList.iterator();
			while(iterator.hasNext()) {
				Movie movie = (Movie) iterator.next();
				if(movie.equals(tempMovie)) {
					favMovieList.add(movie);
					favoriteList.put(userId, favMovieList);
					System.out.println(favoriteList);
					break;
				}
			}
			
		}else {
			List<Movie> favMovieList = favoriteList.get(userId);
			Movie tempMovie = new Movie();
			tempMovie.setId(movieId);
			Iterator iterator = movieList.iterator();
			while(iterator.hasNext()) {
				Movie movie = (Movie) iterator.next();
				if(movie.equals(tempMovie)) {
					favMovieList.add(movie);
					favoriteList.put(userId, favMovieList);
					System.out.println(favoriteList);
					break;
				}
			}
		}
	}
	
	public void removeFavorite(String userId,int movieId) {
		
		if(favoriteList.containsKey(userId)) {
			List<Movie> favMovieList = favoriteList.get(userId);
			Movie tempMovie = new Movie();
			tempMovie.setId(movieId);
			Iterator iterator = favMovieList.iterator();
			while(iterator.hasNext()) {
				Movie movie = (Movie) iterator.next();
				if(movie.equals(tempMovie)) {
					iterator.remove();
					break;
				}
			}
			
			favoriteList.put(userId, favMovieList);
			System.out.println(favoriteList);
		}
	}
	
	public Favorite getFavorite(String userId) {
		if(favoriteList.containsKey(userId)) {
			List<Movie> favMovieList = favoriteList.get(userId);
			Favorite favorite = new Favorite();
			favorite.setMovieList(favMovieList);
			favorite.setCount(favMovieList.size());
			return favorite;
		}
		return null;
	}
 
}
