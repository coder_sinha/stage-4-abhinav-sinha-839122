package com.cognizant.truyum.dao;

import java.util.Iterator;
import java.util.List;

import com.cognizant.truyum.model.MenuItem;

public class MenuItemDaoCollectionImplTest {
	
	
	public static void main(String[] args) {
		
		testGetMenuItemListAdmin();

	}
	
	public static void testGetMenuItemListAdmin() {
		MenuItemDao menuItemDao = new MenuItemDaoCollectionImpl();
		List<MenuItem> menuItemList = menuItemDao.getMenuItemListAdmin();
		Iterator it = menuItemList.iterator();
		
		while(it.hasNext()) {
			MenuItem menuItem = (MenuItem)it.next();
			System.out.println(menuItem.getId()+"\t"+menuItem.getName()+"\t"+menuItem.getPrice()+"\t"+menuItem.isActive()+"\t"+menuItem.getDateOfLaunch()+"\t"+menuItem.getCategory()+"\t"+menuItem.isFreeDelivery());
		}
		
		
	}
	
	public static void testGetMenuItemListCustomer() {
		
	}
	
	public static void testModifyMenuItem() {
		
	}
	
}
