package com.cognizant.springlearn.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.springlearn.model.Employee;

@Component
public class EmployeeDao {

	private static List<Employee> EMPLOYEE_LIST = new ArrayList<>();

	public EmployeeDao() {
		super();
		// TODO Auto-generated constructor stub
		
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		EMPLOYEE_LIST = context.getBean("employeeList",ArrayList.class);
	}
	
	public List<Employee> getAllEmployees() {
		return EMPLOYEE_LIST;
	}
	
}
