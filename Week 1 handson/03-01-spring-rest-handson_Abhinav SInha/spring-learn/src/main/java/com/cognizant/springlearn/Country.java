package com.cognizant.springlearn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Country {

	private String code;
	private String name;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);

	public Country() {
		super();
		// TODO Auto-generated constructor stub
		LOGGER.info("Inside Country Constructor.");
	}

	public String getCode() {
		LOGGER.info("Inside Getter Method For code.");
		return code;
	}

	public void setCode(String code) {
		LOGGER.info("Inside Setter Method For code.");
		this.code = code;
	}

	public String getName() {
		LOGGER.info("Inside Getter Method For name.");
		return name;
	}

	public void setName(String name) {
		LOGGER.info("Inside Setter Method For name.");
		this.name = name;
	}

	@Override
	public String toString() {
		return "Country [code=" + code + ", name=" + name + "]";
	}

}
